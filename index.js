const commandLineArgs = require('command-line-args')
var Client = require('node-rest-client').Client;
const getUsage = require('command-line-usage')
const wales = require('./assets/ansi-wales')

client = new Client();


const optionDefinitions = [
  { name: 'jira', alias: 'j', type: String },
  { name: 'username', alias: 'u', type: String},
  { name: 'password', alias: 'p', type: String},
  { name: 'hours', alias: 't', type: Number }
]
const options = commandLineArgs(optionDefinitions);

if (!options.username || !options.password) {
const sections = [
  {
    header: 'Log Hours for the kindom, work for the people',
    content: 'Right there in the middle we rock and roll.'
  },
  {
    header: 'Options',
    optionList: [
      {
        name: 'jira',
        description: 'The jira number'
      },
      {
        name: 'username',
        description: 'your username'
      },
      {
        name: 'password',
        description: 'your password'
      },
      {
        name: 'hours',
        description: 'the hours you want to log'
      }




    ]
  },
  {
    content: [
      '[italic]{This app was build with Dragons blood.}',
      '',
      wales
    ],
    raw: true
  }
]
const usage = getUsage(sections)
console.log(usage)
return ;
}
// Provide user credentials, which will be used to log in to JIRA.
var loginArgs = {
        data: {
                "username": options.username,
                "password": options.password
        },
        headers: {
                "Content-Type": "application/json"
        } 
};
client.post("https://jira.corp.globant.com/rest/auth/1/session", loginArgs, function(data, response){
        if (response.statusCode == 200) {
                console.log('succesfully logged in, session:', data.session);
                var session = data.session;
                var time = 60 * 60 * (options.hours || 1);
                console.log(time);
                // Get the session information and store it in a cookie in the header
                var searchArgs = {
                        headers: {
								// Set the cookie from the session information
                                cookie: session.name + '=' + session.value,
                                "Content-Type": "application/json"
                        },
                        data: {
                            "comment": "Working working working",
                            "started": "2017-03-27T08:18:02.268+0000",
                            "timeSpentSeconds": time
                        }

                };

                var search = {
                        headers: {
                                                                // Set the cookie from the session information
                                cookie: session.name + '=' + session.value,
                                "Content-Type": "application/json"
                        }

                };
				// Make the request return the search results, passing the header information including the cookie.
                client.post("https://jira.corp.globant.com/rest/api/2/issue/" + options.jira + "/worklog", searchArgs, function(searchResult, response) {
                        console.log('status code:', response.statusCode);
                        console.log('search result:', searchResult);
                });

                /*client.get("https://jira.corp.globant.com/rest/api/2/search?jql=assignee=" + options.username + "&fields=id,key,sprint", search, function(searchResult, response) {
                        console.log('status code:', response.statusCode);
                        console.log(searchResult);
                });*/
        }
        else {
                throw "Login failed :(";
        }
});